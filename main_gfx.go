// Aug 2021
//go:build !no_gfx

// Ackley_mc is for playing with Monte Carlo or simulated annealing on the
// ackley function in N dimensions.
//
//    ackley_mc [input_file]
// where input_file has a list of keywords and values.
// This is mostly the same as the version without graphics, but it does
// not require a command line argument. It will open a graphics window
// so you can type in values and play with it.

package main

import (
	"fmt"
	"os"

	"example.com/ackley_mc/ui"
)

const (
	exitSuccess = iota
	exitFailure
)

func main() {
	if err := ui.MyMain(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(exitFailure)
	}
	os.Exit(exitSuccess)
}
