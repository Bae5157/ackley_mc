# Make a 2D plot of some example results.
# Do not use default axis scaling. It is determined by the most extreme points
# visited and there are always a few outliers. Set the boundaries manually with
# plotmin and plotmax.

input_file  <- "ex2.csv"
plotmin <- -5
plotmax <- 5
getxy <- function(fname) {
    csvnames <- c("step", "fval", "temp", "x1", "x2")
    df <- read.csv(input_file, header = F, col.names = csvnames)
    df  <- df[c("x1", "x2")]
    return(df)
}

# enforce limits keeps just points in which both x1 and x2 are within plot
# limits
enforce_limits <- function (df) {
    df <- df[(df$x1 >= plotmin) & (df$x1 <= plotmax), ]
    df <- df[(df$x2 >= plotmin) & (df$x2 <= plotmax), ]
    return (df)
}

mymain <- function() {
    df <- getxy()
    df <- enforce_limits(df)

    nbins <- 100
    if (F) { # This would be default scaling, but it looks awful.
        x_bin <- seq(floor(min(df$x1)), ceiling(max(df$x1)), length = nbins)
        y_bin <- seq(floor(min(df$x2)), ceiling(max(df$x2)), length = nbins)
    } else {
        x_bin  <- seq(-5, 5, length = nbins)
        y_bin  <- seq(-5, 5, length = nbins)
    }

    freq <-  as.data.frame(table(findInterval(df$x1, x_bin), findInterval(df$x2, y_bin)))
    freq[, 1] <- as.numeric(freq[, 1])
    freq[, 2] <- as.numeric(freq[, 2])

    freq2D <- diag(nbins) * 0
    freq2D[cbind(freq[, 1], freq[, 2])] <- freq[, 3]
    par(mfrow=c(1, 2))
    image(x_bin, y_bin, freq2D, col=topo.colors(max(freq2D)))
    contour(x_bin, y_bin, freq2D, add=TRUE, col=rgb(1,1,1,.7))

    palette(rainbow(max(freq2D)))
    cols <- (freq2D[-1,-1] + freq2D[-1,-(nbins-1)] + freq2D[-(nbins-1),-(nbins-1)] + freq2D[-(nbins-1),-1])/4
    persp(freq2D, col=cols)}

mymain()
