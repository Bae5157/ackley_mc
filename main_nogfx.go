// Aug 2021
//go:build no_gfx
// +build no_gfx

// Ackley_mc is for playing with Monte Carlo or simulated annealing on the
// ackley function in N dimensions.
//
//    ackley_mc input_file
// where input_file has a list of keywords and values.

package main

import (
	"fmt"
	"os"

	"example.com/ackley_mc/mc_work"
)

const (
	exitSuccess = iota
	exitFailure
)

func main() {
	if err := mcwork.MyMain(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(exitFailure)
	}
	os.Exit(exitSuccess)
}
