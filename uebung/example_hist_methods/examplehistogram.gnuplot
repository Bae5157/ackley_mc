# Example histogram for the Uebung.
# If you have the data in a file called in1.csv and the column
# you want is the fourth column, this makes a reasonable histogram.
# Your data is probably in some other column, so change the $4 below.

set datafile separator ','
binwidth=0.005
bin(x,width)=width*floor(x/width)

set title 'distribution of x coords'
set xlab 'x coord'
unset key
plot 'in1.csv' using (bin($4,binwidth)):(1.0) smooth freq with boxes