df <- read.csv(file="in1.csv", header=F,
               col.names=c("step", "temp","function_value", "x1"))
png(filename="ex1_hist.png")
hist (df$x1, xlab = "x coord", main = "", freq = F, breaks = 60)
dev.off()
