#!/bin/sh
# one dimension, and then make a histogram of the distribution
tmp_input=in1
. general_stuff
cat > $tmp_input <<EOF
ini_temp 0.1
final_temp 0.1
n_step 100000
x_ini 0
x_delta 0.5
seed 1637
foutname $tmp_input.csv # This will store all the information from a run.
EOF

$exe $tmp_input

cat > $tmp_input <<EOF
df <- read.csv(file="in1.csv", header=F, col.names=c("step", "temp","function_value", "x1"))
png(filename="plots/ex1.png")
plot (x = df\$step, y = df\$function_value, xlab = "step", ylab = "f value")
png(filename="plots/ex1_hist.png")
hist (df\$x1, xlab = "x coord", main = "", freq = F, breaks = 60)
dev.off()
EOF


R --no-save --no-echo < $tmp_input

