// 15 Feb 2022
// Let us read up an example input file and put its contents into a McPrm
// (monte carlo control parameters) structure.

package ui

import (
	_ "embed"
	"strings"

	"example.com/ackley_mc/mc_work"
)

//go:embed example_input
var exmplInput string

func getExmpl(mcPrm *mcwork.McPrm) error {
	s := strings.NewReader(exmplInput)
	return mcwork.RdPrm(s, mcPrm)
}
