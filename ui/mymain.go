// feb 2022
// We are building a version without graphics
//go:build !no_gfx
// +build !no_gfx

package ui

import (
	"io"
	"os"

	"example.com/ackley_mc/mc_work"
)

// realmain is the real main function. The program wants to read from a file,
// but we can have it read from an io.Reader and then use the test functions
// for playing with it.
func realmain(fp io.Reader) error {
	var mcPrm mcwork.McPrm
	if fp != nil {
		if err := mcwork.RdPrm(fp, &mcPrm); err != nil {
			return err
		}
	} else {
		if err := getExmpl(&mcPrm); err != nil {
			return err
		}
	}
	return UiDoRun(&mcPrm)

}

func MyMain() error {
	if len(os.Args) < 2 {
		return realmain(nil)
	}
	fname := os.Args[1]
	if fp, err := os.Open(fname); err != nil {
		return err
	} else {
		defer fp.Close()
		return realmain(fp)
	}
}
