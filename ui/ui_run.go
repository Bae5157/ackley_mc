// 25 Jan 2020
// Given a buffer or two with a plot picture, send it to the screen

//go:build !no_gfx
// +build !no_gfx

package ui

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"

	"example.com/ackley_mc/mc_work"
)

// Convenience struct so we do not have perverse long parameter lists
type genParams struct {
	mcPrm *mcwork.McPrm   // The MC parameters shared with the main simulation
	win   fyne.Window     // Parent window. Needed for all dialog boxes
	chn   chan workstatus // To tell the output tab current status
}

// We have a channel used to tell the output tab that there are new results.
type status uint8

const (
	calculating  status = iota // currently running a simulation
	resultsReady               // You should collect the results
	errorCalc                  // You should display an error message
)

type workstatus struct {
	mcwork.MCresult
	err    error
	status status // Tells us what we should do now
}

// UiDoRun sets up the screen to do a run. It is effectively a big
// wrapper around the DoRun() function.
func UiDoRun(mcPrm *mcwork.McPrm) error {
	a := app.NewWithID("Monte Carlo")
	win := a.NewWindow("Monte Carlo")
	chn := make(chan workstatus)
	defer close(chn)
	var genParams = genParams{mcPrm, win, chn}
	inputForm := inputTab(genParams)
	quitbutton := widget.NewButton("click somewhere in here to confirm", a.Quit)
	cntrOut := container.NewMax()
	t1 := container.NewTabItem("input tab", inputForm)
	t2 := container.NewTabItem("output tab", cntrOut)
	t3 := container.NewTabItem("quit", quitbutton)
	appTab := container.NewAppTabs(t1, t2, t3)
	win.SetContent(appTab)
	go outputTab(genParams, cntrOut, inputForm)
	win.ShowAndRun()
	return nil
}
