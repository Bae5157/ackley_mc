// feb 2022
// We are building a version without graphics
//go:build no_gfx
// +build no_gfx

// This should mirror the file realmain_gfx.go. It should also remain short.
package mcwork

import (
	"flag"
	"fmt"
	"io"
	"os"
)

// realmain is the real main function. The program wants to read from a file,
// but we can have it read from an io.Reader. This makes testing practical.
func realmain(fp io.Reader) error {
	var mcPrm McPrm
	if err := RdPrm(fp, &mcPrm); err != nil {
		return err
	}
	if _, err := DoRun(&mcPrm); err != nil {
		return err
	}

	return nil
}

// usage
func usage() {
	u := `[input_parameter_file]`
	fmt.Fprintf(flag.CommandLine.Output(), "usage of %s: %s\n", os.Args[0], u)
}

// MyMain is called from the real main. It opens the input file, but passes a
// reader to the function that does the work. This lets us call it from a test
// function.
func MyMain() error {
	if len(os.Args) < 2 {
		usage()
		return fmt.Errorf("No input file given")
	}
	fname := os.Args[1]
	if fp, err := os.Open(fname); err != nil {
		return err
	} else {
		defer fp.Close()
		if err := realmain(fp); err != nil {
			return err
		}
	}
	return nil
}
