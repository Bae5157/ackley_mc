// a utility
package mcwork

import (
	"errors"
	"path/filepath"
)

// setSuffix takes a name and makes sure the desired suffix is at the end
// of the filename.
func setSuffix(fname, suffix string) (string, error) {
	if len(fname) == 0 {
		return "", errors.New("setSuffix given empty fname")
	}
	if suffix == "" { // no suffix might not be an error. Just
		if fname[len(fname)-1] == '.' { // remove any trailing dot
			fname = fname[0 : len(fname)-1]
		}
		if len(fname) > 0 {
			return fname, nil
		}
		return "", errors.New("setSuffix got empty filename")
	}

	if suffix[0] != '.' {
		suffix = "." + suffix
	}
	oldExt := filepath.Ext(fname)
	switch oldExt {
	case suffix:
		return fname, nil
	case "":
		return fname + suffix, nil
	default:
		return fname[0:len(fname)-len(oldExt)] + suffix, nil
	}
}

// removeQuotes removes leading and trailing quotes from a string.
// We do not check for single, unmatched quotes. If you want a file
// called "fname you should use some other program.
func removeQuotes(s string) string {
	for len(s) > 0 && s[0] == '"' {
		s = s[1:]
	}
	for len(s) > 0 && s[len(s)-1] == '"' {
		s = s[:len(s)-1]
	}
	return s
}
