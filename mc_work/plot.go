// 31 Dec 2021
// For plotting. For the moment, this is in the mc_work package,
// but maybe it should be in its own. It definitely goes in its own file, since
// it is tied to one of the chart packages I have tried out.
// What I have learnt about the go-chart package...
//  - The default tick positions are terrible.
//  - If I give it a slice of ticks, it works fine for the main axis, but
//    something terrible happens to the scaling of the axes. I did get
//    it to work by: Make a child type from continuousRange. For this type,
//    define the GetTicks function. go-chart then calls this function, at
//    apparently the right place, and the tick marks come out nicely.
//  - The package puts the main axis on the right and secondary on the left. Yuk
//  - It makes a mess of the scaling on the secondary axis if you do not have
//    a primary axis. This leads to some of the copying and contortions below.
// We make two plots - one for the function value+temperature and one for
// the trajectory of coordinates. We want them to have the same horizontal scale.
// The plot with the temperature has two axes, so the plot package wants to size
// the X-axis differently. The solution is that we draw the temperature on both
// plots, but set all the style attributes to be invisible when we plot the
// X trajectories.
package mcwork

import (
	"fmt"
	"math"
	"os"

	"github.com/wcharczuk/go-chart/v2"
	"github.com/wcharczuk/go-chart/v2/drawing"
	"gitlab.rrz.uni-hamburg.de/Bae5157/axticks"
)

const (
	chrtWdth = 800
	chrtHt   = 400
)

// maketicks gives reasonable default tick locations
func maketicks(axisDscrpt axticks.AxisDscrpt) []chart.Tick {
	xmin, xmax, delta, prcsn := axisDscrpt.Xmin, axisDscrpt.Xmax, axisDscrpt.Delta, axisDscrpt.Prcsn
	rnge := axisDscrpt.Xmax - axisDscrpt.Xmin
	ntick := int(math.Round((rnge / delta) + 1))
	t := make([]chart.Tick, ntick)
	const fstr = "%.*f"
	t[0].Value, t[0].Label = xmin, fmt.Sprintf(fstr, prcsn, xmin)
	for i := 1; i < ntick; i++ {
		t[i].Value = t[i-1].Value + delta
		t[i].Label = fmt.Sprintf("%.*f", prcsn, t[i].Value)
	}
	lastTickVal := t[ntick-1]
	if lastTickVal.Value > xmax {
		t = t[:ntick-1]
	}
	return t
}

// range2 just lets us append methods to a range/continuousrange structure
type range2 struct {
	chart.ContinuousRange
}

// GetTicks is called by go-chart. It has to be a method that acts on a range.
// This means we define a range2 type and define this method on range2. We
// do not use the arguments that go-charts offers (style, formatter, ..)
func (rng *range2) GetTicks(r chart.Renderer, cs chart.Style, vf chart.ValueFormatter) []chart.Tick {
	tmp := []float64{rng.Min, rng.Max}
	if a, err := axticks.Tickpos(tmp); err != nil {
		fmt.Fprintln(os.Stderr, "GetTicks error:", err)
		return nil
	} else {
		rng.Min = a.Xmin
		rng.Max = a.Xmax
		return maketicks(a)
	}
}

// plotbase returns a chart that serves as the basis for both the function
// values and the X trajectories. This lets us set up the x-axis once and
// put the temperature on both plots so they come out the same size.
func plotbase(cprm *cprm) *chart.Chart {
	xaxis := chart.XAxis{
		Name:  "step",
		Range: &range2{},
	}
	var tmprtrAxis chart.YAxis
	if cprm.coolme { // If we are not cooling, we do not plot
		tmprtrAxis = chart.YAxis{ // the temperature axis.
			Name:      "temperature",
			NameStyle: chart.Style{TextRotationDegrees: 360},
			Range:     &range2{},
		}
	} else { // If you do not put something here, go-chart seems
		tmprtrAxis = chart.YAxis{}        // to mess up the scaling on the
		tmprtrAxis.Style = chart.Hidden() //  other axis
	}
	xdata := cprm.plotnstp
	leftAxis := chart.YAxis{
		NameStyle: chart.Style{TextRotationDegrees: 360}, // Zero does not work
		AxisType:  chart.YAxisSecondary,
		Range:     &range2{},
	}
	graph := chart.Chart{
		Width: chrtWdth, Height: chrtHt,
		Series: []chart.Series{
			chart.ContinuousSeries{
				Name:    "temperature",
				XValues: xdata,
				YValues: cprm.plotTmprtr,
			},
			chart.ContinuousSeries{ // The function values
				Name:    "specify me",
				YAxis:   chart.YAxisSecondary,
				XValues: xdata,
				Style: chart.Style{
					StrokeWidth: 4,
					DotWidth:    0,
				},
			},
		},
		YAxis:          tmprtrAxis,
		YAxisSecondary: leftAxis,
		XAxis:          xaxis,

		Background: chart.Style{
			Padding:   chart.Box{Left: 75, Right: 75},
			FillColor: drawing.ColorTransparent,
		},
	}
	return &graph
}

// plotfWrt plots the function values and temperature to an io.Writer that
// it finds in the cprm structure. It might be writing to a file or a buffer.
func plotfWrt(cprm *cprm) error {
	graph := plotbase(cprm)
	graph.Title = "cost function"
	graph.YAxisSecondary.Name = "cost\n(arb units)"
	if funcSeries, ok := graph.Series[1].(chart.ContinuousSeries); ok {
		funcSeries.Name = "cost"
		funcSeries.YValues = cprm.plotf
		graph.Series[1] = funcSeries
	} else {
		panic("prog bug type assertion function plot")
	}

	if err := graph.Render(chart.PNG, cprm.fplotWrt); err != nil {
		return fmt.Errorf("Render: %w", err)
	}

	return nil
}

// For each dimension, allocate space. The X's are stored in a single
// array, so here we allocate an array of arrays for each dimension and
// copy the elements. We then take the graph object and toss out the first
// continuousSeries. Then, we make a series of ContinousSeries, appending
// each in turn to the []graph.Series.
func plotxWrt(cprm *cprm, ndim int) error {
	if cprm.xplotWrt == nil {
		return nil
	}
	graph := plotbase(cprm)
	len_used := len(cprm.plotnstp)
	xdata := make([][]float64, ndim)
	for i := 0; i < ndim; i++ { // Copies, but also does the type promotion
		xdata[i] = make([]float64, len_used) // to double prec for go-chart
	}
	var n int
	for i := 0; i < len_used; i++ {
		for j := 0; j < ndim; j++ {
			xdata[j][i] = float64(cprm.plotXtrj[n])
			n++
		}
	}

	graph.Title = fmt.Sprintf("X trajectories %d dimension", ndim)
	graph.YAxisSecondary.Name = "X coord"

	if b, ok := graph.Series[0].(chart.ContinuousSeries); !ok {
		panic("program bug type assertion on series 0")
	} else {
		b.Style.StrokeColor = chart.ColorTransparent
		b.Style.FillColor = chart.ColorTransparent
		graph.Series[0] = b
	}
	graph.YAxis.Style.FontColor = chart.ColorTransparent
	graph.YAxis.Style.StrokeColor = chart.ColorTransparent
	graph.YAxis.NameStyle.FontColor = chart.ColorTransparent

	if baseSeries, ok := graph.Series[1].(chart.ContinuousSeries); !ok {
		panic("program bug, type assertion on y axis xtrajectories")
	} else {
		graph.Series = graph.Series[:1]
		for i := 0; i < ndim; i++ {
			tmp := baseSeries
			tmp.Name = fmt.Sprintf("dim %d", i)
			tmp.YValues = xdata[i]
			graph.Series = append(graph.Series, tmp)
		}
	}
	if err := graph.Render(chart.PNG, cprm.xplotWrt); err != nil {
		return fmt.Errorf("plotting X trajectories: %w", err)
	}
	return nil
}
