// Aug 2021
//go:build no_gfx
// +build no_gfx

package mcwork

import (
	"fmt"
	"os"
	"strings"
	"testing"
)

var tstTmpDir string

func init() {
	var err error
	if tstTmpDir, err = os.MkdirTemp(".", "test_tmp*"); err != nil {
		fmt.Fprintln(os.Stderr, "Failed to create temporary directory", tstTmpDir)
		os.Exit(1)
	}
}

func TestErr(t *testing.T) {
	rdrErr := strings.NewReader("\nrubbish")
	if err := realmain(rdrErr); err == nil {
		t.Fatal("line with rubbish should provoke error")
	}
}

var sTypo = `
ini_temp = 1
final_temp 0
x_ini 0.8,0.8,0.8
x_delta, "0.4"
n_step = 100000`

func TestTypo(t *testing.T) { // Make sure a typo in input does provoke an error
	if err := realmain(strings.NewReader(sTypo)); err == nil {
		t.Fatal("line with typo should provoke error\n", err, "\nInput", sTypo)
	}
}

type gentest struct{ params, basename string }

var set1 = []gentest{
	{`
ini_temp 1e-10
final_temp 1e-12
x_ini 1,1,1
n_step 10000`, "cold"},
	{`
ini_temp 1
final_temp 0.05
x_ini 15
n_step 10000`, "s1d"},
	{`
ini_temp 0.02
final_temp 0.001
x_ini 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7
n_step 100
x_delta 1`, "s16d"},
	{`
ini_temp 1
final_temp 1
x_ini 1,1,1
n_step 10000`, "hot"},
	{`
ini_temp 0.18
final_temp 0.01
x_ini 7,1,7
n_step 100000`, "phasetrans"},
}

// addTestPath makes some of the loops below a bit more readable
func addTestPath(s string) string {
	return tstTmpDir + string(os.PathSeparator) + s
}

// add OutNames uses the basename field and makes output names for
// the value and x trajectory plot files and the csv file
func addOutNames(s gentest) string {

	ret := s.params
	for _, ss := range []string{"xPltName", "fOutName"} {
		ret += "\n" + ss + " " + addTestPath(s.basename)
	}
	ret += "\nfPltName" + " " + addTestPath(s.basename) + "_fval"
	return ret + "\n"
}

func Test1(t *testing.T) {
	for _, s := range set1 {
		instring := addOutNames(s)
		if err := realmain(strings.NewReader(instring)); err != nil {
			t.Fatal("set1 failed with\n", err, "\nInput:", s)
		}
	}
}

func Test2(t *testing.T) {
	for _, s := range set1 {
		if err := realmain(strings.NewReader(s.params)); err != nil {
			t.Fatal("set1 test2 fail with \n", err, "\nInput:", s)
		}
	}
}

func TestSetSuffix(t *testing.T) {
	var tdata = []struct {
		in, suffix, want string
	}{
		{"boo.", ".csv", "boo.csv"},
		{"boo", "csv", "boo.csv"},
		{"boo.", "csv", "boo.csv"},
		{"boo", ".csv", "boo.csv"},
		{"boo.csv", ".csv", "boo.csv"},
		{"/boo/blah/boo", ".csv", "/boo/blah/boo.csv"},
		{"/a/b/c.svg", ".png", "/a/b/c.png"},
		{"boo.", "", "boo"},
		{"boo", "", "boo"},
	}

	for _, s := range tdata {
		if tmp, err := SetSuffix(s.in, s.suffix); err != nil {
			t.Fatal("setSuffix error handling", s)
		} else {
			if tmp != s.want {
				t.Fatal("Wanted", s.want, "got", tmp, "working on", s)
			}
		}
	}
}

func TestRemoveQuotes(t *testing.T) {
	var tdata = []struct {
		in, out string
	}{
		{"hello", "hello"},
		{"", ""},
		{`"`, ""},
		{`""`, ""},
		{`"""`, ""},
		{`""a"`, "a"},
		{`""aa"`, "aa"},
		{`""a""`, "a"},
		{`"a"`, "a"},
		{`a"`, "a"},
		{`a""`, "a"},
	}
	for _, ss := range tdata {
		if out := RemoveQuotes(ss.in); out != ss.out {
			t.Fatalf("Wanted <%s>, got <%s>. Input was <%s>", ss.out, out, ss.in)
		}
	}
}

// Check that errors are generated
func TestSetSuffixErr(t *testing.T) {
	var tdata = []struct{ in, suffix string }{
		{"", ""},
		{".", ""}}
	for _, s := range tdata {
		if _, err := SetSuffix(s.in, s.suffix); err == nil {
			t.Fatal("setSuffix should return error on", s)
		}
	}
}
