# This only makes sense for local testing of some of the results
# We can plot out the column f_val as a function of temperature, x-coordinates, ...

names <- c("step", "temperature", "f_val", "x1", "x2", "x3", "x4", "x5", "x6", "x7", "x8", "x9", "x10")
df  <- read.csv("testanneal5d.csv", col.names = names)


plot (xlab = "step", ylab = "f val", x = df$step, y = df$f_val,
      type = "p")

