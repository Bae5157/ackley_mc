// Aug 2021

package mcwork

// This is the structure which holds everything we read in to control
// a Monte Carlo run.
// Most things have to be exported so the graphical interface can get
// to them.
type McPrm struct {
	IniTmp, FnlTmp float64   // Initial and final temperatures
	XIni           []float32 // initial x slice
	XDlta          float64   // change x by up to +- xDlta in trial moves
	NStep          int       // number of steps
	NEquil         int       // num steps for equilibration
	NRun           int       // number of separate runs
	fOutName       string    // where we write output to
	fPltName       string    // where we plot to (function values)
	xPltName       string    // where we plot x trajectories to
	verbose        bool
}

// For random numbers. It is not in the main structure, since, if one ever writes
// a threaded version, each thread can have its own value.
var Seed int

// For returning results
type MCresult struct {
	Fdata, Xdata []byte    // png images of function and X data
	NStep, NAcc  int       // number of steps and accepted steps
	Bestfval     float64   // best function value found
	BestX        []float32 // location of this best value found
	LastX        []float32 // Last location. Only used after equilibration.
}

// breaker is for convenience - set breakpoints when debugging.
func breaker(...interface{}) {}
