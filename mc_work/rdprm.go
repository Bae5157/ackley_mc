// Aug 2021
// Read the parameters for a calculation
// We have magic keywords like ini_temp.
// These are stored in a map along with string values. These are then converted
// to floats or ints and put into a structure or variable.
// This is a superset of the requirements in the Uebung. We
//   - hop over blank lines
//   - ignore case
//   - allow comments - anything after a hash (#)

package mcwork

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"strconv"
	"strings"
)

var cmdDflt = []struct {
	key string
	v   string
}{
	{"ini_temp", "20"},
	{"final_temp", ""}, // no default. Use ini_temp if not set
	{"n_step", "1000"},
	{"n_equil", "0"},
	{"n_run", "1"},
	{"x_ini", "3,4,5"},
	{"x_delta", "0.5"},
	{"seed", "1637"},
	{"foutname", ""},
	{"fpltname", ""}, // empty means no plots of function
	{"xpltname", ""},
	{"verbose", ""},
}

// digest digests the map, fills out our structure and sets the seed.
func digest(prmMap map[string]string, mcPrm *McPrm) error {
	var err error
	getf64 := func(s string) float64 {
		if err != nil {
			return 0.0
		}
		var r float64
		r, err = strconv.ParseFloat(s, 64)
		return r
	}
	geti := func(s string) int {
		if err != nil {
			return 0
		}
		var u int64
		u, err = strconv.ParseInt(s, 10, 32)
		return int(u)
	}
	getx := func(s string) []float32 {
		if err != nil {
			return nil
		}
		y := strings.Split(s, ",")
		if len(y) < 1 {
			err = fmt.Errorf("broke trying to get initial x values")
			return nil
		}

		x := make([]float32, len(y))
		for i, s := range y {
			r := 0.
			if r, err = strconv.ParseFloat(s, 32); err != nil {
				return nil
			}
			x[i] = float32(r)
		}
		return x
	}
	mcPrm.IniTmp = getf64(prmMap["ini_temp"])
	mcPrm.FnlTmp = getf64(prmMap["final_temp"])
	mcPrm.XDlta = getf64(prmMap["x_delta"])
	mcPrm.NStep = geti(prmMap["n_step"])
	mcPrm.NEquil = geti(prmMap["n_equil"])
	mcPrm.NRun = geti(prmMap["n_run"])
	mcPrm.XIni = getx(prmMap["x_ini"])
	mcPrm.fOutName = prmMap["foutname"]
	mcPrm.fPltName = prmMap["fpltname"]
	mcPrm.xPltName = prmMap["xpltname"]
	Seed = geti(prmMap["seed"])
	if err != nil { // Only returns the first error encountered
		e := errors.New("Parsing input parameters gave: " + err.Error())
		return e
	}
	if prmMap["verbose"] != "" {
		mcPrm.verbose = true
	}
	return nil
}

// spew spews out an error and known input parameters
func spew() string {
	s := `Input file should be of
           key value
pairs. Values for x_ini are separated by commas without spaces.
`
	for _, ss := range cmdDflt {
		s += fmt.Sprintf("%s %s\n", ss.key, ss.v)
	}
	return s
}

// afterHash takes a string and removes anything after a hash (#) character
func afterHash(s string) string {
	if i := strings.IndexByte(s, '#'); i == -1 {
		return s
	} else {
		return s[:i]
	}
}

// RdPrm reads parameters from a io reader and fills out an control structure.
func RdPrm(fp io.Reader, mcPrm *McPrm) error {
	prmMap := make(map[string]string)
	for _, s := range cmdDflt {
		prmMap[s.key] = s.v // Put default values into map
	}
	scn := bufio.NewScanner(fp)
	for scn.Scan() {
		s := scn.Text()
		s = afterHash(s)
		if len(s) == 0 {
			continue // blank lines
		}
		s = strings.ToLower(s)
		ss := strings.Fields(s)
		if len(ss) < 2 {
			return fmt.Errorf("Only one field in line: %s\n%s", s, spew())
		}
		key := ss[0]
		if _, ok := prmMap[key]; !ok {
			return fmt.Errorf("unknown keyword \"%s\"\n%s", key, spew())
		} else {
			prmMap[key] = ss[1]
		}
	}
	if prmMap["final_temp"] == "" {
		prmMap["final_temp"] = prmMap["ini_temp"]
		if prmMap["verbose"] != "" {
			fmt.Println("setting final temp to ini_temp")
		}
	}
	if err := scn.Err(); err != nil {
		return errors.New(spew())
	}
	return digest(prmMap, mcPrm)
}
