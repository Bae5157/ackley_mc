# Use the normal go build system for everything, but we want to automate some
# simple commands. Unlike most makefiles, there are no dependencies for the
# executables. We let go's build commands take care of that.
.POSIX:

GO=go
GOFMT=gofmt

# The lint target only makes sense for Andrew's setup.
LINTER=~/go/bin/linux_amd64/golangci-lint

WASM_EXE=bin/ackley_wasm.exe

all: $(WASM_EXE)
	GOOS=linux GOARCH=amd64 $(GO) build -o bin/ .
	GOOS=linux GOARCH=amd64 $(GO) build -o bin/ackley_nogfx  -tags no_gfx --  .
	GOOS=darwin GOARCH=amd64 $(GO) build -o bin/ackley_darwinamd64_nogfx -tags no_gfx -- .
	GOOS=darwin GOARCH=arm64 $(GO) build -o bin/ackley_darwinarm64_nogfx -tags no_gfx -- .
	GOOS=windows GOARCH=amd64 $(GO) build -o bin/ackley_nogfx.exe -tags no_gfx -- .


win:
	GOOS=windows GOARCH=amd64 $(GO) build -o bin -- .

# This is not finished, but eventually, we have to play with a webassembly
# version.
$(WASM_EXE):
	GOOS=js GOARCH=wasm $(GO) build -o bin/ackley_wasm.exe -- .

test:
	$(GO) test ./...
	$(GO) test -tags no_gfx ./...

gofmt:
	$(GOFMT) -s -w .

lint:
	$(LINTER) run

clean:
	$(GO) clean
	rm -rf bin/*
	rm -rf examples/*.csv
	rm -rf *.csv
	rm -rf */*_delme.*
	rm -rf */test_tmp*
	rm -rf /tmp/go-build[0-9]*
	rm -rf /tmp/go.*.mod
	rm -rf /tmp/go.*.sum
	rm -rf ./examples/*.csv
	rm -rf ./ackley/*delme*
	rm -rf ./ackley/*jpg
