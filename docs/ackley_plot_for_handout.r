# This only works in two dimensions, but it is enough for the Uebung
# description
ackley <- function(x, y, a, b, c) {
    d <- 2

    sum1 <- x * x + y * y
    sum2 <- cos(c * x) + cos(c * y)

    term1 <- -a * exp(-b * sqrt(sum1 / d))
    term2 <- -exp(sum2 / d)
    y <- term1 + term2 + a + exp(1)
    return(y)
}

mymain <- function() {
    a <- 20; b <- 0.2; c <- 2 * pi; #   defaults
    x <- seq(from = -4, to = 4, length.out = 200)
    z <- outer(X = x, Y = x, FUN = ackley, a, b, c)

    lim <- c(min(x), max(x))
    if (T) {
        png(filename = "ackley2d.png", bg = "white")
    }
    persp(x = x, y = x, z = z,
          xlab = "x1", ylab = "x2", zlab = "ackley",
          theta = -20,
          ticktype = "detailed")
    dev.off()
}

mymain()
