// Dec 2021

/*
ackley_mc is for teaching. We use Monte Carlo to sample Ackley's function
in N dimensions or perhaps with simulated annealing to optimise it.

# Running

To run it
  ackley_mc [ input_file ]
or
  ackley_mc input_file
if it was not compiled with the graphical interface. The difference is that the input_file is not optional when there is no graphical interface.

If you are running with the graphical interface, you do not need an input file and some default values will be loaded. 


# Input parameters

These are defined in rdprm.go. The defaults look like
	ini_temp  1
	final_temp ""
	n_step  1000
	n_equil  0
	x_ini  3,4,5
	x_delta  0.5
	seed  1637
	foutname  ""
	fpltname  ""
	xpltname  ""
	adaptstep  ""
	verbose  0

and they have the meaning
	ini_temp     initial temperature
	final_temp   final temperature. If this is set, we are doing annealing. If it is not set, it will be the same as ini_temp, and this will be a constant temperature run.
	n_step       number of steps
	n_equil      number of steps of equilibration. The Monte Carlo routine is called twice. The first time, with this many steps. The results are then thrown away, since they were only there for equilibration.
	x_ini        initial values for x. It is a comma separated list with no spaces allowed. It is used to determine the number of dimension. A value like "1" would give you one dimension whose initial value is 1. A value like "-1,0,1,2" would give you a four dimensional system with initial values at -1, 0, 1 and 2.
	x_delta  0.5 used to decide on the maximum step size
	seed  1637   seed for the random number generator
	foutname  ""
	fpltname  ""
	xpltname  ""
	verbose  "" if empty, do not be too verbose

If a final_temp is not specified, set the final temp to be the same as the initial temperature and do a run at constant temperature.
If final_temp is lower than initial temperature, we will do simulated annealing.

foutname is where a csv file will be written with values for plotting.

xpltname is where a png file will be written with a plot of coordinates.

Anything in the file after a hash (#) symbol will be treated as a comment.

Any items which are mis-spelled will provoke an error.

# Output

If you are running with the graphical interface, the emphasis is on quickly playing with parameters. There are buttons which let you dump a plot to a file in .png format.

Running without the graphical interface, you should specify `foutname.csv` and you will get a .csv file that you can feed into almost any plotting program. The format on each line looks like

 0,0.1,5.8874,-1.5,0.5,1
 1,0.1,5.9918,-1.73,0.5,1
 2,0.1,5.9968,-1.73,0.514,1

which is
 - step number
 - temperature
 - function value
 - x1, x2, x3, ...

So if you want to plot the function value as a function of step, you would plot column 1 on the x-axis and column 3 on the y-axis. To plot the coordinates in the example above, you would plot columns 4, 5 and 6 on the y-axis.

*/
package ackley_doc
