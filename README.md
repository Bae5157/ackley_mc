# Monte Carlo / Simulated Annealing for Ackleys function

Given Ackleys function in *n* dimensions and some arbitrary starting point, run a Monte Carlo / simulated annealing calculation to find an optimum.. or maybe not. That is the point. You can play with things like the temperature to see when the system converges and when it behaves badly.

# Install and run

I do not have a mac for testing, so this is limited to linux and windows. It is possible that the instructions for linux are helpful for a mac. We can cross compile for a Mac, but not with graphics. The makefile is useful to me under linux. It makes a pile of binaries and sticks them in the bin directory.

## Prerequisites
 - You must have go installed. I have only tried with go back to version 1.16. It probably builds with earlier versions. It requires support for `go.mod`.
 - You need a C compiler for the graphics.
 - cross compiling is easy without graphics, but not much fun if you want graphics.
 - native compilation under windows is painless


## Build
    git clone git@gitlab.rrz.uni-hamburg.de:Bae5157/ackley_mc.git
    cd ackley_mc
    go build .

and you should end up with an executable called `ackley_mc` or `ackley_mc.exe` under windows. To build without graphics,

    git clone git@gitlab.rrz.uni-hamburg.de:Bae5157/ackley_mc.git
    cd ackley_mc
    go build -tags no_gfx .
 
### Graphics yes / no ?

The graphical interface makes it convenient to play with parameters. You can quickly see what happens if you raise the initial temperature by a factor of 10.  The graphical interface does not make beautiful plots. One can export the data and plot it with gnuplot/excel/R/whatever.

## Binaries
Binaries for linux and windows are linked to from the moodle page. If you download a linux binary, you will have to `chmod +x ackley_mc` to make it executable.

# Documentation
The real documentation is in `go doc` format.

# Plotting output
This is in csv format. For gnuplot, do not forget to `set datafile separator ','`.

# BUGS
## Real Bugs

### Best value
Check if this is real. When doing simulated annealing, we print out the best value found and the step number. I think the step number might be wrong.

### Only under windows
I use `os.Create()` to open a file for writing (plots, `.csv` files. In my "Documents" folder, the function call returns "The system cannot find the file specified". On my test machine, this was a result of trying to write to a protected folder with an application (ackley_mc) that windows did not know about. If you fight your way through the windows menus, you will find a place that shows "blocked software". You could add `ackley_mc.exe` to the list of blessed executables, but why not just run the code from another directory?

## Non bugs

The UI toolkit is "fyne". It does not do charts, so we use "go-chart". This makes a `.png` which we store in memory and feed to fine. I did try `.svg` filetypes, but an .svg made by go-chart causes fyne to choke. This means we are stuck with .png. One day, one should try out the charting library which is based on fyne.

It is not much fun typing numbers into the input form. We just use the functions from the toolkit, but the cursor and decimal point seem to jump around like a vegetarian in a butchers shop.

The graphical interface follows no known style convention and looks like it was designed by a three-year old.

Still have not implemented parallel runs, but given the emphasis on the quick graphics, this may not be appropriate.

Should put a button on the parameter tab to let us write the parameters on the page to a file.
