// Put ackley in here, so we can also write a little test, separate from
// the Monte Carlo code.

package ackley

import (
	"math"
)

const (
	a float64 = 20
	b         = 0.2
	c         = 2 * math.Pi
)

// ackley calculates the n-dimensional ackley function for a slice x.
//The constants are hard-coded above.
func Ackley(x []float32) float64 {
	var sum1, sum2 float64
	for i := range x {
		xx := float64(x[i])
		sum1 += xx * xx
	}

	sum1 /= float64(len(x))
	sum1 = math.Sqrt(sum1)
	sum1 *= -b
	sum1 = -a * math.Exp(sum1)
	for i := range x {
		sum2 += math.Cos(c * float64(x[i]))
	}
	sum2 /= float64(len(x))
	sum2 = math.Exp(sum2)
	t := sum1 - sum2 + a + math.E
	return t

}
