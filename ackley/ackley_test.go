// 5 Aug 2021
// It is a bit hard to really test, but I can write a file that we
// can feed to something like gnuplot.

package ackley_test

import (
	"fmt"
	"io"
	"os"
	"testing"

	. "example.com/ackley_mc/ackley"
)

// A gridsize has a min and max and the number of increments between
type grdsz struct {
	xmin, xmax float32
	ninc       int
}

func TestAckleyNd(t *testing.T) {
	const tol = 1e-10
	for i := 0; i < 10; i++ {
		x := make([]float32, i)
		y := Ackley(x)
		if y < 0 {
			y = -y
		}
		if y > tol {
			t.Errorf("Ackley with %d dimension, got %g wanted 0", i, y)
		}
	}
}

func loopOver(x []float32, idim int, fOut io.Writer, grid grdsz) {
	idim--
	xinc := (grid.xmax - grid.xmin) / float32(grid.ninc-1)

	for i := 0; i < grid.ninc; i++ {
		x[idim] = grid.xmin + float32(i)*xinc
		if idim > 0 {
			loopOver(x, idim, fOut, grid)
		}

		if i == grid.ninc-1 {
			if !(i == len(x)-1 && idim == len(x)-1) {
				return
			}
		}
		fmt.Fprintf(fOut, "%g", Ackley(x))
		for _, r := range x {
			fmt.Fprintf(fOut, ",%g", r)
		}
		fmt.Fprintln(fOut)
	}

}

func TestAckley1d(t *testing.T) {
	plotNotes := `# plotme with gnuplot plot1d.gplt
    set datafile sep ","
	set xlabel 'x'
	set ylabel "ackley"
	set term "jpeg"
	set output "test1d.jpg"

	plot 'test1d_delme.dat' us 2:1 with line`
	const fOutName = "test1d_delme.dat"
	const plotNoteName = "plot1d_delme.gplt"
	grid := grdsz{-10, 10, 500}

	if fp, err := os.Create(fOutName); err != nil {
		t.Errorf("fail create %s", fOutName)
	} else {
		defer fp.Close()
		inc := (grid.xmax - grid.xmin) / float32(grid.ninc)
		var xx [1]float32
		for xx[0] = grid.xmin; xx[0] <= grid.xmax; xx[0] += inc {
			fmt.Fprintf(fp, "%g,%g\n", Ackley(xx[:]), xx[0])
		}
	}

	if fp, err := os.Create(plotNoteName); err != nil {
		t.Errorf("opening %s", plotNoteName)
	} else {
		if _, err := fp.Write([]byte(plotNotes)); err != nil {
			t.Errorf("Writing to %s", plotNoteName)
		}
	}
}

// TestAckley2d
func TestAckley2d(t *testing.T) {
	plotNotes := `# plotme with gnuplot plot2d.gplt
    set datafile sep ","
	set xlab 'x'; set ylab 'y'
	set zlabel "ackley"
	set term "jpeg"
	set output "test2d.jpg"

	splot 'test2d_delme.dat' us 2:3:1 with points`
	const fOutName = "test2d_delme.dat"
	const plotNoteName = "plot2d_delme.gplt"
	grid := grdsz{-5, 5, 40}

	var ioWrt io.Writer
	if fp, err := os.Create(fOutName); err != nil {
		t.Errorf("fail")
	} else {
		defer fp.Close()
		ioWrt = fp
	}
	x := make([]float32, 2)
	loopOver(x, 2, ioWrt, grid)
	if fp, err := os.Create(plotNoteName); err != nil {
		t.Errorf("opening %s", plotNoteName)
	} else {
		if _, err := fp.Write([]byte(plotNotes)); err != nil {
			t.Errorf("Writing to %s", plotNoteName)
		}
	}
}
